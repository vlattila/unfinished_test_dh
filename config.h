#pragma once

#include <algorithm>
#include <bitset>
#include <iostream>
#include <string>
#include <vector>



class Config
{
private:
    std::string m_path;
    std::string m_debugger;
    std::vector<std::string> m_config;

public:
    Config(std::string &path)
    {
        m_path.empty() ? m_path = "default.conf" : m_path = path;
    };

    void Read()
    {
        std::ifstream file(m_path);
        std::string line;
        while (std::getline(file, line))
        {
            m_config.emplace_back(line);
        }
    };

    void getConfig()
    {
        for(auto i : m_config)
        {
            std::cout << i << std::endl;
        }
    };

    void print_ip_binary(const int &in)
    {
        std::cout << std::bitset<32>(in) <<std::endl;
    }
    int getNetwork(const std::string &ip_address)
    {
        struct in_addr ip_addr;

        std::cout << ip_address << std::endl;
        inet_pton(AF_INET, ip_address.c_str(), &ip_addr);
        
        print_ip_binary(ip_addr.s_addr);
        print_ip_binary(1<<24);
        int netmask;
        if (ip_addr.s_addr & 1 << netmask)
        {
            std::cout << "Hit the range" <<std::endl;
        }
        else
        {
            std::cout << "Free way" << std::endl;
        }
        return 0;
    }

    int isNetworkMember(const std::string &ip_address)
    {
        struct in_addr ip_addr, range_addr, subnet_mask;
        inet_pton(AF_INET, ip_address.c_str(), &ip_addr);
        std::cout << "ip address:" << ip_address << std::bitset<32>(ip_addr.s_addr) << "\n" <<std::endl;
        for (std::string i : m_config)
        {

            int subnet_mask_length = std::atoi(i.substr(i.find('/')+1, i.size()).c_str());
            int subnet_mask_address = std::atoi(i.substr(i.find('/')-1).c_str());
            std::cout << subnet_mask_address << "/"<< subnet_mask_address << std::endl; 
            inet_pton(AF_INET, i.c_str(), &range_addr);

            uint32_t mask = (0xFFFFFFFF << (32 - subnet_mask_length)) & 0xFFFFFFFF;

            uint32_t address_mask = mask & ip_addr.s_addr;

            uint32_t network_ip = ip_addr.s_addr & mask;
            uint32_t network_range = range_addr.s_addr & mask;
            std::cout << "network from config: " << std::bitset<32>(ip_addr.s_addr) << " network ip: " << std::bitset<32>(network_ip) << "  network_range: " << std::bitset<32>(network_range) << std::endl; 
            
            if (address_mask & mask == mask)
            {
                std::cout << "IP RANGE HIT" << std::endl;
                return true;
            }
        }
        return false;
    };
};