#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <arpa/inet.h>

#include <cstring>
#include <string>
#include <iostream>
#include <fstream>

#include "config.h"

#define PORT 8000
#define THREAD_NUM 5
#define MAX_LINE_LENGTH 256

int ssock;
pthread_mutex_t smutex = PTHREAD_MUTEX_INITIALIZER;

void *comm_th(void *arg)
{
    Config *config = static_cast<Config *>(arg);
    int ix;
    int csock;
    std::string buf;
    int end = 1;
    struct sockaddr_storage cli_addr;
    std::string ip_addr;
    socklen_t cli_addr_len = sizeof(cli_addr);
    while (end)
    {
        pthread_mutex_lock(&smutex);
        csock = accept(ssock, (struct sockaddr *)&cli_addr, &cli_addr_len);
        pthread_mutex_unlock(&smutex);

        switch (cli_addr.ss_family)
        {
        case AF_INET:
            char str_addr4[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, &(((struct sockaddr_in *)&cli_addr)->sin_addr), str_addr4, INET_ADDRSTRLEN);
            config->isNetworkMember(str_addr4);
            break;
        case AF_INET6:
            char str_addr[INET6_ADDRSTRLEN];
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)&cli_addr)->sin6_addr), str_addr, INET6_ADDRSTRLEN);
            config->isNetworkMember(str_addr);
            break;
        default:
            break;
        }

        sleep(3);
        while (end)
        {
            buf.clear();
            buf.resize(255);
            read(csock, &buf[0], 255);
            std::cout << "<" << ip_addr << " " << buf << std::endl;
            send(csock, buf.c_str(), buf.size(), 0);
            std::cout << ">" << ip_addr << " " << buf << std::endl;
        }

        close(csock);
    }
    return NULL;
}

int main()
{
    std::string path{};
    Config config(path);
    config.Read();
    struct sockaddr_in addr;
    pthread_t th[THREAD_NUM];

    int i;

    int reuse;
    int *pix;

    if ((ssock = socket(PF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
        return 1;
    }

    reuse = 1;
    setsockopt(ssock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

    std::memset(&addr, 0, sizeof(addr));

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");;
    addr.sin_port = htons(PORT);
    
    if (bind(ssock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        return 1;
    }

    if (listen(ssock, 5) < 0)
    {
        perror("listen");
        return 1;
    }

    for (i = 0; i < THREAD_NUM; i++)
    {
        pthread_create(&th[i], NULL, comm_th, &config);
    }

    while (1)
        sleep(1);

    return 0;
}