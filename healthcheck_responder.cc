/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <algorithm>
#include <functional>
#include <numeric>
#include <string>
#include <vector>
#include <map>
#include <cstdlib>
#include <iostream>
#include <sys/epoll.h>

#define MAX 1024


 struct check_command {
        std::string path;
        std::vector<char*> args;
        std::string stdin_input;
        uint8_t expected_return_code;
        std::string expected_stdout;
        uint8_t command_timeout;
        uint8_t check_interval;

        std::string getCommand()
        {
            std::string res {path+ " -"};
            res.append(std::accumulate(args.begin(), args.end(), std::string{}));
            return res;
        }

};

struct Response {
    public:
        int status;
        std::string resp;
    Response(int status, std::string resp): status{status}, resp{resp} {}
};

class Mconfig {
    public:
        std::map<std::string, check_command> check_commands;
        std::string listen_ip; // "::1"
        uint16_t listen_port; // 80
        std::string resp_success{"200 OK\n"};
        std::string resp_failed{"200 FAIL\n"};
};


void log(const std::string &msg)
{
    std::cout << "[LOG]" << msg << std::endl;
}

void error(const char *msg)
{
    std::cout << "[ERR]" << msg << std::endl;
    exit(1);
}

class httpParser {
    public:
        static std::string get_parser(std::string str) {
            auto first_pos_it = std::find(str.begin(), str.end(), '/');
            if (first_pos_it == str.end()) return ""; // No '/'
            auto end_path = std::find(first_pos_it, str.end(), ' ');
            auto second_pos_it = std::find(first_pos_it + 1, end_path - 1, '/');
            if (second_pos_it == str.end()) return ""; // Only one '/'
            return std::string(&*second_pos_it + 1,end_path - second_pos_it - 1);
        }
};

class Healthcheck {
  public:
    Healthcheck(){};
    int check(check_command& command)
    {
        epfd = epoll_create1(0);
        ev.events = EPOLLIN;
        ev.data.fd = STDIN_FILENO; // for quit
        int ret = epoll_ctl(epfd, EPOLL_CTL_ADD, STDIN_FILENO, &ev);
    }
  private:
    int run(check_command& command)
    {
        int ret;
        const char* cptr_command = command.getCommand().c_str();

        pid_t pdf = fork();
        if (pdf == -1)
        {
            error("Fork error");
            exit(1);
        }else if(pdf == 0)
        {
            const char* args = "";
            int ret_val = system(cptr_command);
            ev.data.fd = pdf;
            ret = epoll_ctl(epfd, EPOLL_CTL_ADD, pdf, &ev);
            
        }
    }
    struct epoll_event ev;
    struct epoll_event evlist[MAX];
    int epfd = 0;
};


void filler(Mconfig& conf)
{
    check_command a {"./test1.sh", {"l","a","r","t","h"},"", 0,"", 50,10};
    check_command b {"./test.sh",  {"valami"},"", 0,"", 50,10};
    conf.check_commands.insert({"ls", a});
    conf.check_commands.insert({"test", b});
}

int main(int argc, char *argv[])
{
    Mconfig config;
    filler(config);
    Healthcheck hcheckker;
    int sockfd, newsockfd;
    socklen_t clilen;
    char req[1025];
    std::string resp;
    struct sockaddr_in serv_addr, cli_addr;
    size_t n; 
    int opt;
    sockfd = socket(AF_INET, SOCK_STREAM , 0);
    if (sockfd < 0) 
       error("ERROR opening socket");
    log("socket created");
    if ((setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR | SO_REUSEPORT, &opt,sizeof(int))) == -1)
	{
		close(sockfd);
		printf("ERROR  SWM : Set Socket ReUSED ERROR \n");
		return 0;
	}
    log("socket ops added");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    config.listen_ip = "127.0.0.1";
    inet_aton(config.listen_ip.c_str(), &serv_addr.sin_addr);
    serv_addr.sin_port = htons(8080);

    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
        close(sockfd);
        error("ERROR on binding");
    }
    log("Bind");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    log("Server started");
       newsockfd = accept(sockfd, 
                   (struct sockaddr *) &cli_addr, 
                   &clilen);
       if (newsockfd < 0) 
            error("ERROR on accept");
       
    while(1)
    {
       bzero(req,1024);
       resp.clear();
       n = read(newsockfd, req,1024);
       if( n < 0)
       {
        log("Read error");
       }
        std::cout << "BUFF:\n" << req << std::endl;
       std::string app(httpParser::get_parser(req));
       if (!app.empty())
       {
            hcheckker.check(config.check_commands[app]);
       }
       resp = config.resp_success.c_str();
       n = write(newsockfd, resp.c_str() ,resp.size());
       printf("RESP:%s\n", resp.c_str());
       if( n != resp.size())
       {
        log("Read error");
       }
    }
    close(newsockfd);
    close(sockfd);
    return 0; 
}
